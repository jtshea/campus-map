# CAMPUS MAP
[Online Demo](http://sheaworks.com/campus-map/)

## PROPOSED CONCEPT
Imagine a map application not unlike Google Maps, but simpler. There is a high-resolution map-layer image that serves as a flat background for the app. Users can pan around the map. There are several pins for buildings that are overlayed on top of the map image. Users can also zoom in and out of the map to get a more detailed or more general view. The entire application is responsive, so will behave more or less the same on a phone, tablet, or desktop.

## ACCOMPLISHED
- Load arbitrary map background from URL.
- Auto-detect map background dimensions.
- Map drag.
- Map constrained to container.
- Arbitrary container positioning.
- Container height based on map proportions.
- Zoom by button click.
- Zoom animation.
- Map zooms while retaining relative center point.
- Pin / map object system.
- Pin positions zoom / scale with map.
- Map responsive to viewport width.
- Touch device drag.

## TO DO
### Code
- Track / manage map state with hashtag variable.
- Add custom pins through url variables.
- Double-click to zoom/reposition map.
- Scroll-wheel to zoom.
- Pin click action.
- More robust pin data source.
- Pin object types (pins, buildings, text box).
- Keep map centered during viewport width changes.
- Display zoom amount.
- Zoom slider.
- Clickable pin list to reposition map on a pin.

### Design
- CSS styles and options.

### Docs
- Write answers to some questions about accomplished / envisioned solution:
  - How you would implement the map viewport. 
  - How would you design the viewport and coordinate system? What assumptions would you make? 
  - What issues would you need to consider to achieve this, and how does your solution address those issues? 
  - How does that concept translate into your code? 
  - What CSS, JS, HTML, device, or other features would you leverage to accomplish this?

## MAYBE
- Make this a jQuery plugin with focus on ease of use.
- Paths: show suggested route from one point to another.
- For fun, subtle animations of random pedestrians, cars, birds moving on the map along logical paths.
