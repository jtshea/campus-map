//settings / preferences
var mapBackground = "http://www.mappery.com/maps/University-of-Tennessee-Chattanooga-Campus-Map.gif";
var mapID = "#map";
var containerID = "#container";
var mapScaleMax = 1.5;
var containerMaxWidth = 600;
var containerMinWidth = 320;
var zoomLevels = 5;

//calculated persisent variables
var zoomStep = mapScaleMax / zoomLevels;
var mapScaleMin;
var mapScale;
var mapImageWidth;
var mapImageHeight;

//other persistent variables
var timeout = 0;

//find the map image dimensions
var imgLoad = $("<img />");
imgLoad.attr("src", mapBackground + "?" + new Date().getTime());
imgLoad.unbind("load");
imgLoad.bind("load", function () {
    mapImageWidth = this.width;
    mapImageHeight = this.height;
});

//quick and dirty pin array
//to-do: better data structure, pin setting utility
// left, right, text
var pins = [[480,580,"pin 1"],[200,100,"pin 2"],[1000,800,"pin 3"]];

//dom is loaded
$(document).ready(function(){
    initialize();               
});


function initialize(){
    
    //wait for the map dimensions
    if(typeof mapImageHeight == 'undefined'){
        if(timeout > 50){ //we've waited too long
            debug("map image timeout");
            return;
        }
        setTimeout(function(){initialize()},100);//keep waiting
        timeout++;
        return;
    }
                                
    //set initial container and map display dimensions
    var initialMapHeight = mapImageHeight * containerMaxWidth / mapImageWidth;
    $(containerID).width(containerMaxWidth).height(initialMapHeight);
    $(mapID).width(containerMaxWidth).height(initialMapHeight);
    
    //calculate zoom and scale values
    mapScaleMin = $( containerID ).width( ) / mapImageWidth;
    mapScale = mapScaleMin;
    
    //create pins
    var arrayLength = pins.length;
    for (var i = 0; i < arrayLength; i++) {
        var pinID = "obj"+i;
        var $divHtml = "<div id='"+pinID+"' class='mapobject pin shadow' onmousedown='event.stopPropagation();pinClick("+i+")'></div>";
        $(mapID).append($divHtml);
        $(pinID).css("left", pins[i][0]+"px");
        $(pinID).css("top", pins[i][1]+"px");

    }
        
    
    //set up user controls
    $( "#zoomin" ).mousedown(function() {
      zoom(zoomStep);
    });

    $( "#zoomout" ).mousedown(function() {
      zoom(zoomStep * -1);
    });
    
    //to do: implement double click to center/zoom
    //to do: implement scroll wheel zoom
         
    mapUpdate();//initial refresh
    mapResize();//initial resize
    
    //assign map background to map layer
    $(mapID).css("background-image", "url(" + mapBackground + ")");
    
    //show container and buttons
    $(containerID).show();
    $("#buttoncontainer").show();
    
    $( window ).resize(mapResize);//bind resize function
    
    //convert mouse events to touch events
    //see function touchHandler(event)
    var themap = document.getElementById("map");
    themap.addEventListener("touchstart", touchHandler, true);
    themap.addEventListener("touchmove", touchHandler, true);
    themap.addEventListener("touchend", touchHandler, true);
    themap.addEventListener("touchcancel", touchHandler, true); 
           
}


function zoom(mapScaleChange){
    var oldMapScale = mapScale;
    mapScale += mapScaleChange;
    if(mapScale < mapScaleMin || mapScale < zoomStep) mapScale = mapScaleMin;
    if(mapScale > mapScaleMax) mapScale = mapScaleMax;
    if(mapScale==oldMapScale){
        return;
    }else{
        var mapCursor = (mapScale == mapScaleMin) ? "default" : "crosshair";
        $(mapID).css("cursor", mapCursor);
        mapUpdate();
    }    
}


function mapUpdate(){
    
    if($(mapID).data('ui-draggable')) $(mapID).draggable('disable'); //no dragging during update
         
    var containerWidth = $(containerID).width();
    var containerHeight = $(containerID).height();
    var mapLeft = $(mapID).position().left;
    var mapTop = $(mapID).position().top;    
    var mapWidthOld = $(mapID).width();
    var mapHeightOld = $(mapID).height();        
    var mapWidthNew = mapImageWidth * mapScale;    
    var mapHeightNew = mapImageHeight * mapScale;
                   
    //http://stackoverflow.com/questions/8678374/javascript-jquery-scale-an-image-to-its-containers-center-point
    
    //adjust for center
            
        //vertical  
        var differenceY = $(containerID).offset().top - $(mapID).offset().top;
        var centerY = differenceY + containerHeight / 2;
        var centerYPercent = centerY / mapHeightOld;
        var heightChangePercent = mapHeightNew / mapHeightOld;
        var newY = heightChangePercent * mapHeightOld;       
        var mapTopNew = -centerYPercent * newY + containerHeight / 2;
        
        //horizontal 
        var differenceX = $(containerID).offset().left - $(mapID).offset().left;
        var centerX = differenceX + containerWidth / 2;
        var centerXPercent = centerX / mapWidthOld;
        var widthChangePercent = mapWidthNew / mapWidthOld;
        var newX = widthChangePercent * mapWidthOld;
        var mapLeftNew = -centerXPercent * newX + containerWidth / 2;
    
        //adjust edges
        
        //right edge
        if(mapWidthNew + mapLeftNew < containerWidth){
            mapLeftNew = -mapWidthNew + containerWidth;
        }
        
        //left edge
        if(mapLeftNew > 0) mapLeftNew = 0;
    
        //bottom edge
        if(mapHeightNew + mapTopNew < containerHeight){
            mapTopNew = -mapHeightNew + containerHeight;
        }
        
        //top edge
        if(mapTopNew > 0) mapTopNew = 0;

        
    //update the display, nesting some events
    $(".mapobject").hide(0, function() { //keep pins from hopping around
        
        //adjust pin position to map scale
        var arrayLength = pins.length;
        for (var i = 0; i < arrayLength; i++) {
            var pinLeft = pins[i][0] * mapScale;
            var pinTop = pins[i][1] * mapScale;
            $("#obj"+i).css("left", pinLeft+"px");
            $("#obj"+i).css("top", pinTop+"px");
        }
                                    
        $(mapID).animate({//move map to new coords
        "left": mapLeftNew, "height": mapHeightNew,
        "width": mapWidthNew, "top": mapTopNew }, 200, function() {
                
            $(".mapobject").show(0, function () {
                
                draggingContainment();//update dragging containment
                $(mapID).draggable('enable');//allow dragging again
                
            });//pins can come back now
            
        }); 
                                                       
    });
                                    
}

function draggingContainment(){//update dragging containment
//item needs to be visible to get offset
    var x1 = $(containerID).width() - $(mapID).width() + $(containerID).offset().left;
    var y1 = $(containerID).height() - $(mapID).height() + $(containerID).offset().top;
    var x2 = $(containerID).offset().left;
    var y2 = $(containerID).offset().top;    
    $(mapID).draggable({containment:[x1,y1,x2,y2]});//this is relative to window!!
}

//dynamic map resize
function mapResize(){
    var viewportWidth = $(window).width();
    var containerWidth = $(containerID).width();
    var containerHeight = $(containerID).height();
    var mapWidth = $(mapID).width();
    var mapHeight = $(mapID).height();
    $(containerID).css("width", viewportWidth-40);
    setTimeout(function(){draggingContainment()},0);//update dragging containment    
}

function debug(ourtext){
    $( "#debug" ).append( ourtext + "<br/>" );   
}

function pinClick(pin){
    $pinText = pins[pin][2];
    alert($pinText);
}

/* http://stackoverflow.com/questions/5345762/jquery-mobile-drag-and-drop */
//convert mouse events to touch events
function touchHandler(event)
{
    var touches = event.changedTouches,
        first = touches[0],
        type = "";
         switch(event.type)
    {
        case "touchstart": type = "mousedown"; break;
        case "touchmove":  type="mousemove"; break;        
        case "touchend":   type="mouseup"; break;
        default: return;
    }
 
    var simulatedEvent = document.createEvent("MouseEvent");
    simulatedEvent.initMouseEvent(type, true, true, window, 1, 
                              first.screenX, first.screenY, 
                              first.clientX, first.clientY, false, 
                              false, false, false, 0/*left*/, null);
    first.target.dispatchEvent(simulatedEvent);
    event.preventDefault();
}
